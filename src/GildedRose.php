<?php

declare(strict_types=1);

namespace GildedRose;

use GildedRose\Item\AgedBrie\AgedBrieItem;
use GildedRose\Item\BackstagePass\BackstagePassItem;
use GildedRose\Item\Conjured\ConjuredItem;
use GildedRose\Item\SulfurasLegendary\SulfurasLegendaryItem;

final class GildedRose
{
    /**
     * @var array<Item>
     */
    private array $items;

    private GildedRoseRegistry $registry;

    /**
     * @param array<Item> $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;

        $this->registry = new GildedRoseRegistry();
        $this->registry->set('Aged Brie', new AgedBrieItem());
        $this->registry->set('Sulfuras, Hand of Ragnaros', new SulfurasLegendaryItem());
        $this->registry->set('Backstage passes to a TAFKAL80ETC concert', new BackstagePassItem());
        $this->registry->set('Conjured Mana Cake', new ConjuredItem());
    }

    public function updateQuality(): void
    {
        foreach ($this->items as $item) {
            $this->registry->updateQuality($item);
        }
    }
}
