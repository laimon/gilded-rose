<?php

declare(strict_types=1);

namespace GildedRose;

use GildedRose\Item\Common\CommonItem;
use GildedRose\Item\ItemTemplate;

class GildedRoseRegistry
{
    private array $services = [];

    public function __construct()
    {
        $this->set('default', new CommonItem());
    }

    public function set(string $key, ItemTemplate $item): void
    {
        $this->services[$key] = $item;
    }

    public function get(string $key): ItemTemplate
    {
        if (! isset($this->services[$key])) {
            $key = 'default';
        }

        return $this->services[$key];
    }

    public function updateQuality(Item $item): void
    {
        $service = $this->get($item->name);
        $service->setItem($item);
        $service->updateQuality();
    }
}
