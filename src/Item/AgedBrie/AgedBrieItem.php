<?php

declare(strict_types=1);

namespace GildedRose\Item\AgedBrie;

use GildedRose\Item\ItemTemplate;

class AgedBrieItem extends ItemTemplate
{
    public function updateQuality(): void
    {
        $this->decrementSellIn();

        if ($this->isQualityUnderMax()) {
            $this->changeQuality(1);
        }

        if ($this->isSellInEnd() && $this->isQualityUnderMax()) {
            $this->changeQuality(1);
        }
    }
}
