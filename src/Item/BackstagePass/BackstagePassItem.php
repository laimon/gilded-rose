<?php

declare(strict_types=1);

namespace GildedRose\Item\BackstagePass;

use GildedRose\Item\ItemTemplate;

class BackstagePassItem extends ItemTemplate
{
    public function updateQuality(): void
    {
        if ($this->isQualityUnderMax()) {
            $this->changeQuality(1);

            if ($this->getSellIn() < 11 && $this->isQualityUnderMax()) {
                $this->changeQuality(1);
            }

            if ($this->getSellIn() < 6 && $this->isQualityUnderMax()) {
                $this->changeQuality(1);
            }
        }

        $this->decrementSellIn();

        if ($this->isSellInEnd()) {
            $this->setQuality(0);
        }
    }
}
