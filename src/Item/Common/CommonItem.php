<?php

declare(strict_types=1);

namespace GildedRose\Item\Common;

use GildedRose\Item\ItemTemplate;

class CommonItem extends ItemTemplate
{
    public function updateQuality(): void
    {
        $this->decrementSellIn();

        if ($this->isQualityOverMin()) {
            $this->changeQuality(-1);
        }

        if ($this->isSellInEnd() && $this->isQualityOverMin()) {
            $this->changeQuality(-1);
        }
    }
}
