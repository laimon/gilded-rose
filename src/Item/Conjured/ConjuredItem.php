<?php

declare(strict_types=1);

namespace GildedRose\Item\Conjured;

use GildedRose\Item\ItemTemplate;

class ConjuredItem extends ItemTemplate
{
    public function updateQuality(): void
    {
        $this->decrementSellIn();

        if ($this->isQualityOverMin()) {
            $this->changeQuality(-2);
        }

        if ($this->isSellInEnd() && $this->isQualityOverMin()) {
            $this->changeQuality(-2);
        }
    }
}
