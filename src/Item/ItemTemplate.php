<?php

declare(strict_types=1);

namespace GildedRose\Item;

use GildedRose\Item;

class ItemTemplate
{
    public const QUALITY_MAX = 50;

    public const QUALITY_MIN = 0;

    public Item $item;

    public function setItem(Item $item): self
    {
        $this->item = $item;
        return $this;
    }

    public function getName(): string
    {
        return $this->item->name;
    }

    public function setName(string $name): self
    {
        $this->item->name = $name;
        return $this;
    }

    public function getSellIn(): int
    {
        return $this->item->sell_in;
    }

    public function setSellIn(int $sellIn): self
    {
        $this->item->sell_in = $sellIn;
        return $this;
    }

    public function getQuality(): int
    {
        return $this->item->quality;
    }

    public function setQuality(int $quality): self
    {
        $this->item->quality = $quality;
        return $this;
    }

    public function decrementSellIn(int $days = 1): self
    {
        $sellIn = $this->getSellIn() - $days;
        $this->setSellIn($sellIn);
        return $this;
    }

    public function changeQuality(int $changeBy): self
    {
        $quality = $this->getQuality() + $changeBy;
        $this->setQuality($quality);
        return $this;
    }

    public function updateQuality(): void
    {
    }

    protected function isQualityUnderMax(): bool
    {
        return $this->getQuality() < self::QUALITY_MAX;
    }

    protected function isQualityOverMin(): bool
    {
        return $this->getQuality() > self::QUALITY_MIN;
    }

    protected function isSellInEnd(): bool
    {
        return $this->getSellIn() < 0;
    }
}
