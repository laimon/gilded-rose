<?php

declare(strict_types=1);

namespace Tests;

use GildedRose\GildedRoseRegistry;
use GildedRose\Item;
use GildedRose\Item\AgedBrie\AgedBrieItem;
use PHPUnit\Framework\TestCase;

class GildedRoseRegistryTest extends TestCase
{
    public function testGildedRoseRegistry(): void
    {
        $registry = new GildedRoseRegistry();
        $registry->set('Aged Brie', new AgedBrieItem());

        $item = new Item('Aged Brie', 2, 0);

        $registry->updateQuality($item);
        $this->assertSame(1, $item->sell_in);
        $this->assertSame(1, $item->quality);
    }
}
