<?php

declare(strict_types=1);

namespace Tests;

use GildedRose\GildedRose;
use GildedRose\Item;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    /**
     * @dataProvider getItemData
     */
    public function testGildedRose(array $items, int $expectSellIn, int $expectQuality): void
    {
        $app = new GildedRose($items);
        $app->updateQuality();
        $item = $items[0];
        $this->assertSame($expectSellIn, $item->sell_in);
        $this->assertSame($expectQuality, $item->quality);
    }

    public function getItemData(): iterable
    {
        $item = new Item('+5 Dexterity Vest', 10, 20);
        yield 'testUpdateQuality' => [[$item], 9, 19];
    }
}
