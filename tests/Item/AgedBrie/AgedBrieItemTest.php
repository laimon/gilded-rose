<?php

declare(strict_types=1);

namespace Tests\Item\AgedBrie;

use GildedRose\Item;
use GildedRose\Item\AgedBrie\AgedBrieItem;
use GildedRose\Item\ItemTemplate;
use PHPUnit\Framework\TestCase;
use Tests\Item\ItemDataTest;

class AgedBrieItemTest extends TestCase
{
    /**
     * @dataProvider getAgedBrieItemData
     */
    public function testAgedBrieItem(AgedBrieItem $agedBrieItem, int $expectSellIn, int $expectQuality): void
    {
        ItemDataTest::assertItemData($this, $agedBrieItem, $expectSellIn, $expectQuality);
    }

    public function getAgedBrieItemData(): iterable
    {
        $agedBrieItem = $this->createAgedBrieItem('Aged Brie', 2, 0);
        yield 'testUpdateQuality' => [$agedBrieItem, 1, 1];

        $agedBrieItem = $this->createAgedBrieItem('Aged Brie', 0, 0);
        yield 'testSellInZeroItem' => [$agedBrieItem, -1, 2];

        $agedBrieItem = $this->createAgedBrieItem('Aged Brie', 0, 50);
        yield 'testMaxQualityItem' => [$agedBrieItem, -1, 50];
    }

    private function createAgedBrieItem(string $name, int $sellIn, int $quality): ItemTemplate
    {
        $item = new Item($name, $sellIn, $quality);
        return (new AgedBrieItem())->setItem($item);
    }
}
