<?php

declare(strict_types=1);

namespace Tests\Item\BackstagePass;

use GildedRose\Item;
use GildedRose\Item\BackstagePass\BackstagePassItem;
use GildedRose\Item\ItemTemplate;
use PHPUnit\Framework\TestCase;
use Tests\Item\ItemDataTest;

class BackstagePassItemTest extends TestCase
{
    /**
     * @dataProvider getBackstagePassItemData
     */
    public function testBackstagePassItem(
        BackstagePassItem $backstagePassItem,
        int $expectSellIn,
        int $expectQuality
    ): void {
        ItemDataTest::assertItemData($this, $backstagePassItem, $expectSellIn, $expectQuality);
    }

    public function getBackstagePassItemData(): iterable
    {
        $backstagePassItem = $this->createBackstagePassItem('Backstage passes to a TAFKAL80ETC concert', 15, 20);
        yield 'testUpdateQuality' => [$backstagePassItem, 14, 21];

        $backstagePassItem = $this->createBackstagePassItem('Backstage passes to a TAFKAL80ETC concert', 10, 20);
        yield 'testLessElevenDaysItem' => [$backstagePassItem, 9, 22];

        $backstagePassItem = $this->createBackstagePassItem('Backstage passes to a TAFKAL80ETC concert', 5, 20);
        yield 'testLessSixDaysItem' => [$backstagePassItem, 4, 23];

        $backstagePassItem = $this->createBackstagePassItem('Backstage passes to a TAFKAL80ETC concert', 0, 20);
        yield 'testSellInEndItem' => [$backstagePassItem, -1, 0];
    }

    private function createBackstagePassItem(string $name, int $sellIn, int $quality): ItemTemplate
    {
        $item = new Item($name, $sellIn, $quality);
        return (new BackstagePassItem())->setItem($item);
    }
}
