<?php

declare(strict_types=1);

namespace Tests\Item\Common;

use GildedRose\Item;
use GildedRose\Item\Common\CommonItem;
use GildedRose\Item\ItemTemplate;
use PHPUnit\Framework\TestCase;
use Tests\Item\ItemDataTest;

class CommonItemTest extends TestCase
{
    /**
     * @dataProvider getCommonItemData
     */
    public function testCommonItemTest(CommonItem $commonItem, int $expectSellIn, int $expectQuality): void
    {
        ItemDataTest::assertItemData($this, $commonItem, $expectSellIn, $expectQuality);
    }

    public function getCommonItemData(): iterable
    {
        $commonItem = $this->createCommonItem('+5 Dexterity Vest', 10, 20);
        yield 'testUpdateQuality' => [$commonItem, 9, 19];

        $commonItem = $this->createCommonItem('+5 Dexterity Vest', 0, 20);
        yield 'testSellInZeroItem' => [$commonItem, -1, 18];

        $commonItem = $this->createCommonItem('+5 Dexterity Vest', 10, 0);
        yield 'testQualityZeroItem' => [$commonItem, 9, 0];
    }

    private function createCommonItem(string $name, int $sellIn, int $quality): ItemTemplate
    {
        $item = new Item($name, $sellIn, $quality);
        return (new CommonItem())->setItem($item);
    }
}
