<?php

declare(strict_types=1);

namespace Tests\Item\Conjured;

use GildedRose\Item;
use GildedRose\Item\Conjured\ConjuredItem;
use GildedRose\Item\ItemTemplate;
use PHPUnit\Framework\TestCase;
use Tests\Item\ItemDataTest;

class ConjuredItemTest extends TestCase
{
    /**
     * @dataProvider getConjuredItemData
     */
    public function testConjuredItem(ConjuredItem $conjuredItem, int $expectSellIn, int $expectQuality): void
    {
        ItemDataTest::assertItemData($this, $conjuredItem, $expectSellIn, $expectQuality);
    }

    public function getConjuredItemData(): iterable
    {
        $conjuredItem = $this->createConjuredItem('Conjured Mana Cake', 10, 20);
        yield 'testUpdateQuality' => [$conjuredItem, 9, 18];

        $conjuredItem = $this->createConjuredItem('Conjured Mana Cake', 0, 20);
        yield 'testSellInZeroItem' => [$conjuredItem, -1, 16];

        $conjuredItem = $this->createConjuredItem('Conjured Mana Cake', 10, 0);
        yield 'testQualityZeroItem' => [$conjuredItem, 9, 0];
    }

    private function createConjuredItem(string $name, int $sellIn, int $quality): ItemTemplate
    {
        $item = new Item($name, $sellIn, $quality);
        return (new ConjuredItem())->setItem($item);
    }
}
