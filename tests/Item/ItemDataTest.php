<?php

declare(strict_types=1);

namespace Tests\Item;

use GildedRose\Item\ItemTemplate;
use PHPUnit\Framework\TestCase;

class ItemDataTest
{
    public static function assertItemData(
        TestCase $case,
        ItemTemplate $item,
        int $expectSellIn,
        int $expectQuality
    ): void {
        $item->updateQuality();

        $case->assertSame($expectSellIn, $item->getSellIn());
        $case->assertSame($expectQuality, $item->getQuality());
    }
}
