<?php

declare(strict_types=1);

namespace Tests\Item;

use GildedRose\Item;
use GildedRose\Item\ItemTemplate;
use PHPUnit\Framework\TestCase;

class ItemTemplateTest extends TestCase
{
    public function testItemTemplate(): ItemTemplate
    {
        $item = new Item('+5 Dexterity Vest', 10, 20);
        $itemTemplate = new ItemTemplate();
        $itemTemplate->setItem($item);

        $this->assertInstanceOf(ItemTemplate::class, $itemTemplate);

        $this->assertSame('+5 Dexterity Vest', $itemTemplate->getName());
        $this->assertSame(10, $itemTemplate->getSellIn());
        $this->assertSame(20, $itemTemplate->getQuality());

        return $itemTemplate;
    }

    /**
     * @depends testItemTemplate
     */
    public function testSetName(ItemTemplate $itemTemplate): void
    {
        $newName = '+10 Dexterity Vest';
        $itemTemplate->setName($newName);
        $this->assertSame($newName, $itemTemplate->getName());
    }

    /**
     * @depends testItemTemplate
     */
    public function testSetQuality(ItemTemplate $itemTemplate): void
    {
        $newQuality = 19;
        $itemTemplate->setQuality($newQuality);
        $this->assertSame($newQuality, $itemTemplate->getQuality());
    }

    /**
     * @depends testItemTemplate
     */
    public function testSetSellIn(ItemTemplate $itemTemplate): void
    {
        $newSellIn = 9;
        $itemTemplate->setSellIn($newSellIn);
        $this->assertSame($newSellIn, $itemTemplate->getSellIn());
    }

    /**
     * @depends testItemTemplate
     */
    public function testDecrementSellIn(ItemTemplate $itemTemplate): void
    {
        $oldSellIn = $itemTemplate->getSellIn();
        $days = 1;
        $itemTemplate->decrementSellIn($days);
        $this->assertSame($oldSellIn - $days, $itemTemplate->getSellIn());
    }

    /**
     * @depends testItemTemplate
     */
    public function testChangeQuality(ItemTemplate $itemTemplate): void
    {
        $oldQuality = $itemTemplate->getQuality();
        $changeBy = 2;
        $itemTemplate->changeQuality($changeBy);
        $this->assertSame($oldQuality + $changeBy, $itemTemplate->getQuality());
    }
}
