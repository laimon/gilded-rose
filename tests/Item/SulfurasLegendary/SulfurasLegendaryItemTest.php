<?php

declare(strict_types=1);

namespace Tests\Item\SulfurasLegendary;

use GildedRose\Item;
use GildedRose\Item\SulfurasLegendary\SulfurasLegendaryItem;
use PHPUnit\Framework\TestCase;
use Tests\Item\ItemDataTest;

class SulfurasLegendaryItemTest extends TestCase
{
    public function testUpdateQuality(): void
    {
        $item = new Item('Sulfuras, Hand of Ragnaros', 0, 80);
        $sulfurasLegendaryItem = (new SulfurasLegendaryItem())->setItem($item);

        ItemDataTest::assertItemData($this, $sulfurasLegendaryItem, 0, 80);
    }
}
