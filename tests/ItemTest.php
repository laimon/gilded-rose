<?php

declare(strict_types=1);

namespace Tests;

use GildedRose\Item;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{
    public function testItem(): void
    {
        $item = new Item('+5 Dexterity Vest', 10, 20);
        $this->assertInstanceOf(Item::class, $item);

        $this->assertSame($item->__toString(), '+5 Dexterity Vest, 10, 20');
    }
}
